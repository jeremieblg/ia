from sklearn.linear_model import LinearRegression
from sklearn.datasets import make_regression
import matplotlib.pyplot as plt
import numpy as np
from math import sqrt

x_test, y_test, coeff = make_regression(200, 1, noise=50, random_state=1, coef=True)


def find_cost(a, x_points, y_points):
    if len(x_points) != len(y_points):
        raise Exception("Invalid parameters")

    total = 0
    for index, x_point in enumerate(x_points):
        # Calculate f(x)
        predicted_y = a * x_point[0]

        diff = predicted_y - y_points[index]
        squared_diff = diff ** 2

        total += squared_diff

    return total / len(x_points)


def find_derivative_cost(a, x_points, y_points):
    if len(x_points) != len(y_points):
        raise Exception("Invalid parameters")

    total = 0
    for index, x_point in enumerate(x_points):
        # Calculate f(x)
        predicted_y = a * x_point[0]

        diff = (predicted_y - y_points[index]) * x_point[0]

        total += diff

    return (2 * total) / len(x_points)


a = 0
learning_rate = 0.01
threshold = 0.0001
previous_cost = None

while True:
    cost = find_cost(a, x_test, y_test)
    derivative_cost = find_derivative_cost(a, x_test, y_test)
    a = a - learning_rate * derivative_cost

    if previous_cost is not None and abs(previous_cost - cost) <= threshold:
        break

    previous_cost = cost


print("Estimated: {}. Scikit: {}".format(a, coeff))


rep_x = np.linspace(-700.0, 900.0)

fig, ax = plt.subplots()
ax.grid(linestyle="--")


# TODO: Uncomment to check the cost function.
# for value in rep_x:
#     cost = find_cost(value, x_test, y_test)
#     ax.plot(value, cost, "go")

ax.plot(x_test, y_test, "ro")
ax.plot(rep_x, a * rep_x, "b-")
ax.plot(rep_x, coeff * rep_x, "r-")

plt.show()
