from sklearn.datasets import make_regression
import matplotlib.pyplot as plt
import numpy

x_test, y_test = make_regression(200, 2, noise=200, random_state=1)

x = numpy.array(x_test)
y = numpy.array(y_test)

feature_1 = x[:,0]
feature_2 = x[:,1]

print(feature_1.shape)
print(feature_2.shape)

fig, ax = plt.subplots()
ax.grid(linestyle="--")

ax.plot(feature_1, y_test, "ro")
ax.plot(feature_2, y_test, "bo")
plt.show()
