from sklearn.datasets import make_regression
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
import numpy

# Generation.
x_test, y_test, coeff = make_regression(200, 2, noise=1, random_state=1, coef=True)

# Computing.
linear_regression = LinearRegression().fit(x_test, y_test)

print(coeff)
print(linear_regression.coef_, linear_regression.intercept_)


prediction_x = numpy.linspace(1, 4, 18).reshape(-1, 2)
prediction_y = linear_regression.predict(prediction_x)

print(numpy.array(prediction_y).shape)

# Representation.

x = numpy.array(x_test)
y = numpy.array(y_test)

feature_1 = x[:,0]
feature_2 = x[:,1]

fig, ax = plt.subplots()
ax.grid(linestyle="--")

ax.plot(feature_1, y_test, "ro")
ax.plot(feature_2, y_test, "bo")

ax.plot(prediction_x, prediction_y, "yo")

plt.show()
