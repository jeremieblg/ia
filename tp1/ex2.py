from sklearn.linear_model import LinearRegression
from sklearn.datasets import make_regression
import matplotlib.pyplot as plt
import numpy as np

x_test, y_test, coeff = make_regression(200, 1, noise=50, random_state=1, coef=True)

linear_regression = LinearRegression().fit(x_test, y_test)
print(coeff)
print(linear_regression.coef_, linear_regression.intercept_)


prediction_x = np.linspace(-5, 5, 200).reshape(-1, 1)
prediction_y = linear_regression.predict(prediction_x)


x = np.linspace(-5.0, 5.0)


fig, ax = plt.subplots()
ax.grid(linestyle="--")

ax.plot(x_test, y_test, "ro")

ax.plot(prediction_x, prediction_y, "yo")

ax.plot(x, linear_regression.coef_ * x + linear_regression.intercept_)
ax.plot(x, coeff * x + 0, "g-")

plt.show()
