from sklearn.datasets import make_regression
import matplotlib.pyplot as plt

x_test, y_test = make_regression(200, 1, noise=50)

fig, ax = plt.subplots()
ax.grid(linestyle="--")

ax.plot(x_test, y_test, "ro")
plt.show()
