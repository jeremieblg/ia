from sklearn.datasets import make_blobs
import matplotlib.pyplot as plt
import numpy

X, y = make_blobs(
  n_samples=200,
  n_features=2, 
  random_state=8
)

cluster_0 = X[y == 0]
cluster_1 = X[y == 1]
cluster_2 = X[y == 2]

fig, ax = plt.subplots()
ax.grid(linestyle="--")

ax.plot(cluster_0[:,0], cluster_0[:,1], "bo")
ax.plot(cluster_1[:,0], cluster_1[:,1], "go")
ax.plot(cluster_2[:,0], cluster_2[:,1], "ro")

plt.show()
