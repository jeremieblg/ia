from sklearn.datasets import make_blobs
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import numpy

X, y = make_blobs(
  n_samples=200,
  n_features=2, 
  random_state=8
)


(unique, counts) = numpy.unique(y, return_counts=True)
print("Number of clusters: {}".format(len(counts)))

kmeans = KMeans(init="random", algorithm="full", n_clusters=len(counts))\
  .fit(X)

cluster_0 = X[kmeans.labels_ == 0]
cluster_1 = X[kmeans.labels_ == 1]
cluster_2 = X[kmeans.labels_ == 2]

fig, ax = plt.subplots()
ax.grid(linestyle="--")

ax.plot(cluster_0[:,0], cluster_0[:,1], "bo")
ax.plot(cluster_1[:,0], cluster_1[:,1], "go")
ax.plot(cluster_2[:,0], cluster_2[:,1], "ro")

ax.plot(
  kmeans.cluster_centers_[:,0], kmeans.cluster_centers_[:,1],
  "kx",
  markersize=10
)

plt.show()
