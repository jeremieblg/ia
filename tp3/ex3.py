from sklearn.datasets import make_blobs
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.metrics.pairwise import euclidean_distances
import numpy
import random
from matplotlib import pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.datasets import make_moons, make_circles, make_blobs
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import make_pipeline


def calculate_cost(data, labels, K):
  size = len(data)
  total = 0

  for i in range(1, size):
    # Récupération du centroide uC(i)
    centroide = K[labels[i]]
    total += (data[i] - centroide)**2

  return total / size


def kmean(X, K: int):
  # Boite englobante.
  x = X[:,0]
  y = X[:,1]

  min_x = numpy.amin(x)
  max_x = numpy.amax(x)
  min_y = numpy.amin(y)
  max_y = numpy.amax(y)

  # n centroides aléatoires.
  centroides = []

  for i in range(K):
    rand_x = random.uniform(min_x, max_x)
    rand_y = random.uniform(min_y, max_y)

    centroides.append((rand_x, rand_y))

  last_cost = None

  while True:
    distances = euclidean_distances(X, centroides)
    labels = numpy.argmin(distances, axis=-1)

    # Calcul des nouveaux centroides.
    new_centroides = []

    for i in range(K):
      cluster = X[labels == i]

      if len(cluster) == 0:
        # Personne dans le cluster.
        new_centroides.append((
          random.uniform(min_x, max_x),
          random.uniform(min_y, max_y)
        ))
      else:
        new_centroides.append(numpy.mean(cluster, axis=0))

    cost = calculate_cost(X, labels, centroides)
    print("Cost: {}".format(cost))
  
    if last_cost is not None and numpy.array_equal(cost, last_cost):
      break
    
    last_cost = cost
    centroides = new_centroides

  return numpy.array(centroides), labels


X, y = make_blobs(
  n_samples=200,
  n_features=2, 
  random_state=8
)

(unique, counts) = numpy.unique(y, return_counts=True)
centroides, labels = kmean(X, len(counts))

# Contour plot.
classifier = make_pipeline(
  StandardScaler(),
  MLPClassifier(
      solver='lbfgs', alpha=0.1, random_state=1, max_iter=2000,
      early_stopping=True, hidden_layer_sizes=[100, 100],
  )
)

figure = plt.figure()
h = .02  # step size in the mesh

# split into training and test part
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.4)

x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
xx, yy = numpy.meshgrid(numpy.arange(x_min, x_max, h),
                      numpy.arange(y_min, y_max, h))

# just plot the dataset first
cm = ListedColormap(['#6e6eff', '#a4ffa4', '#ff9393'])
cm_bright = ListedColormap(['#0000FF', '#00FF00', '#FF0000'])
ax = plt.subplot(1, 1, 1)
# Plot the training points
ax.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=cm_bright)
# and testing points
ax.scatter(X_test[:, 0], X_test[:, 1], c=y_test, cmap=cm_bright, alpha=0.6)
ax.set_xlim(xx.min(), xx.max())
ax.set_ylim(yy.min(), yy.max())
ax.set_xticks(())
ax.set_yticks(())

# iterate over classifiers
ax = plt.subplot(1, 2, 1)
ax.grid(linestyle="--")
classifier.fit(X_train, y_train)
score = classifier.score(X_test, y_test)

# Plot the decision boundary. For that, we will assign a color to each
# point in the mesh [x_min, x_max] x [y_min, y_max].

if hasattr(classifier, "predict"):
    Z = classifier.predict(numpy.c_[xx.ravel(), yy.ravel()])
else:
    Z = classifier.predict_proba(numpy.c_[xx.ravel(), yy.ravel()])[:, 1]

# Put the result into a color plot
Z = Z.reshape(xx.shape)
ax.contourf(xx, yy, Z, cmap=cm, alpha=.8)

# Plot also the training points
ax.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=cm_bright, s=25)
# and testing points
ax.scatter(X_test[:, 0], X_test[:, 1], c=y_test, cmap=cm_bright,
            alpha=0.6, s=25)

ax.set_xlim(xx.min(), xx.max())
ax.set_ylim(yy.min(), yy.max())
ax.set_xticks(())
ax.set_yticks(())

ax.plot(
  centroides[:,0], centroides[:,1],
  "kx",
  markersize=10
)

figure.subplots_adjust(left=.02, right=.98)
plt.show()
