from sklearn.datasets import make_classification
from sklearn.linear_model import LogisticRegression
import matplotlib.pyplot as plt
import numpy as np


def split(coordinates, y):
  return (coordinates[y == 1], coordinates[y == 0])


x_test, y_test = make_classification(
  n_samples=200,
  n_features=2,
  n_redundant=0,
  n_informative=2,
  random_state=1
)

logistic_regression = LogisticRegression(penalty="none").fit(x_test, y_test)

random_points = np.random.rand(20, 2) * 8 - 4
prediction = logistic_regression.predict(random_points)


is_true, is_false = split(x_test, y_test)

is_true_prediction, is_false_prediction = split(random_points, prediction)


fig, ax = plt.subplots()
ax.grid(linestyle="--")

# Initial values.
for i in is_true:
  ax.plot(i[0], i[1], "bo")

for i in is_false:
  ax.plot(i[0], i[1], "ro")

# Predictions
for i in is_true_prediction:
  ax.plot(i[0], i[1], "go")

for i in is_false_prediction:
  ax.plot(i[0], i[1], "yo")


plt.show()
