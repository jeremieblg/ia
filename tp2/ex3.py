from math import exp, cos, log
from sklearn.datasets import make_classification
import matplotlib.pyplot as plt
import numpy as np

def logistical_function(o):
  return 1 / (1 + exp(-o))


def f_logistical_function(teta, x):
  # Produit scalaire de teta par x
  scalar = np.dot(teta, x)
  print(x)
  print(scalar)
  return logistical_function(scalar)


def s_err(f_teta, y):
  if y == 1:
    return -log(f_teta)
  elif y == 0:
    return -log(1 - f_teta)
  else:
    raise Exception("\"y\" must be equal to either 1 or 0")


def cost(teta, x, y):
  n = len(x)
  total = 0

  for i in range(n):
    xi = x[i]
    yi = y[i]
    f_teta = f_logistical_function(teta, xi)

    total += s_err(f_teta, yi)

  return total / n


def split(coordinates, y):
  return (coordinates[y == 1], coordinates[y == 0])


x_test, y_test = make_classification(
  n_samples=200,
  n_features=2,
  n_redundant=0,
  n_informative=2,
  random_state=1
)

print(x_test)
print(cost(0, x_test, y_test))


is_true, is_false = split(x_test, y_test)
fig, ax = plt.subplots()
ax.grid(linestyle="--")

for i in is_true:
  ax.plot(i[0], i[1], "bo")

for i in is_false:
  ax.plot(i[0], i[1], "ro")


plt.show()
