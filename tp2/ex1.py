from sklearn.datasets import make_classification
import matplotlib.pyplot as plt
import numpy

x_test, y_test = make_classification(
  n_samples=200,
  n_features=2,
  n_redundant=0,
  n_informative=2,  
  random_state=1
)

is_true = x_test[y_test == 1]
is_false = x_test[y_test == 0]

fig, ax = plt.subplots()
ax.grid(linestyle="--")

for i in is_true:
  ax.plot(i[0], i[1], "bo")

for i in is_false:
  ax.plot(i[0], i[1], "ro")

plt.show()
